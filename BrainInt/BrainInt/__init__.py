# Author: NoOneIsHere
# BrainInt

from sys import argv

CELLS = [0] * 255
CELL_MOD = 256
CELL_NUM = 0

try:
    with open(argv[1]) as openfile:
        data = openfile.readlines()
except:
    print("What should I do?\n")
    data = [] # Without this line, PyCharm complains despite
    exit(1) # This line

def go_left():
    global CELL_NUM
    CELL_NUM -= 1
    CELL_NUM %= len(CELLS)

def go_right():
    global CELL_NUM
    CELL_NUM += 1
    CELL_NUM %= len(CELLS)

def inc():
    CELLS[CELL_NUM] += 1
    CELLS[CELL_NUM] %= CELL_MOD

def dec():
    CELLS[CELL_NUM] -= 1
    CELLS[CELL_NUM] %= CELL_MOD

def dot():
    print(chr(CELLS[CELL_NUM]))

def exc():
    print(CELLS[CELL_NUM])

def mult():
    CELLS[CELL_NUM] *= CELLS[(CELL_NUM + 1) % CELL_MOD]
    CELLS[CELL_NUM] %= CELL_MOD

def div():
    CELLS[CELL_NUM] /= CELLS[(CELL_NUM + 1) % CELL_MOD]
    CELLS[CELL_NUM] = int(CELLS[CELL_NUM])
    CELLS[CELL_NUM] %= CELL_MOD

def mod():
    CELLS[CELL_NUM] %= CELLS[(CELL_NUM + 1) % CELL_MOD]

def at():
    exit(CELLS[CELL_NUM])

def ampersand():
    CELLS[CELL_NUM] = int(input()) % CELL_MOD

def dollar():
    CELLS[CELL_NUM] += CELLS[(CELL_NUM + 1) % CELL_MOD]
    CELLS[CELL_NUM] %= CELL_MOD

def percent():
    CELLS[CELL_NUM] -= CELLS[(CELL_NUM + 1) % CELL_MOD]
    CELLS[CELL_NUM] %= CELL_MOD

def boolean():
    CELLS[CELL_NUM] = int(bool(CELLS[CELL_NUM]))

# Max for cell is 255, min is 0

# Char, What it does
# +, increment current cell
# -, decrement current cell
# >, move one cell to the right
# <, move one cell to the left
# ., output current cell as ASCII char
# !, output current cell as number
# *, multiply cell and cell to the right
# /, integer divide cell and cell on right
# [, start loop (while cell != 0)
# ], end loop
# {, start for loop (exec code, then move cell until cell is zero)
# }, end for loop
# $, add cell and cell on right, clearing cell on right
# %, subtract cell and cell on right, clearing cell on right
# @, end program
# &, set current cell to input given from user
# (, exec code <current cell> number of times
# ), end exec loop


DELEGATOR = {'+': 'inc', '-': 'dec', '>': 'go_right', '<': 'go_left', '.': 'dot', '!': 'exc', '*': 'mult', '/': 'div',
             '[': 'LOOP=True;LOOP_END="]";', ']': '', '$': 'dollar', '%': 'percent', '@': 'at', '&': 'ampersand', '(': 'LOOP=True;LOOP_END=")";', ')': '',
             '#': 'mod', '^': 'boolean'}

def run_code(code):
    global CELL_NUM
    LOOP = False
    LOOP_CONTENT = ''
    LOOP_END = ''
    for line in code:
        for char in line:
            try:
                if LOOP:
                    if char != LOOP_END:
                        LOOP_CONTENT += char
                    else:
                        LOOP = False
                        # Handle loop
                        if char == ')':
                            for i in range(CELLS[CELL_NUM]):
                                run_code([LOOP_CONTENT])
                        elif char == ']':
                            while CELLS[CELL_NUM]:
                                run_code([LOOP_CONTENT])
                        else:
                            while CELLS[CELL_NUM]:
                                run_code([LOOP_CONTENT])
                                CELL_NUM += 1
                                CELL_NUM %= len(CELLS)
                        # Clear loop body
                        LOOP_CONTENT = ''
                else:
                    exec(DELEGATOR[char] + '()')
                    if char == '(':
                        LOOP = True
                        LOOP_END = ')'
                    elif char == '[':
                        LOOP = True
                        LOOP_END = ']'
                    elif char == '{':
                        LOOP = True
                        LOOP_END = '}'
            except KeyError:
                pass # No-Op

run_code(data)